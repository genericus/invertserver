;;;; frontend.lisp
;;;;
;;;; Copyright (c) 2015 Corey Wells

(in-package #:invertserver)

(define-easy-handler (mainpage :uri "/") ()
  (setf (content-type*) "text/html")
  (with-html-output-to-string (s nil :prologue t)
    (:html :lang "en"
           (:head
            (:meta :charset "utf-8")
            (:title "Video Player")
            (:link :rel "stylesheet" :type "text/css" :href "/static/css/style.css")
            (:link :rel "shortcut icon" :href "/favicon.ico"))
           (:body
            (:h1 :id "title" "Inverted Video Player")
            (:select :id "videoselect" :class "blockcenter" :autocomplete "off"
                     (:option :value "noselect" "Choose a video here")
                     (loop for path in (cl-fad:list-directory *video-location*)
                        unless (cl-fad:directory-pathname-p path) do
                          (let* ((f (enough-namestring path (truename *video-location*)))
                                 (fstr (escape-string (namestring f))))
                            (htm
                             (:option :value fstr (str fstr))))))
            (:img :id "throbber" :src "/static/images/throbber.gif"
                  :alt "Loading")
            (:video :id "videoplayer" :width 1000 :height 600
                    :controls t :class "blockcenter" :loop t)
            (:script :type "text/javascript" :src "/static/js/player.js")))))
