;;;; handledispatch.lisp
;;;;
;;;; Copyright (c) 2015 Corey Wells

(in-package #:invertserver)

(defun handle-static-inverted-file (pathname &optional content-type)
  "Like `HANDLE-STATIC-FILE', but applies bitwise NOT before sending."
  (when (or (wild-pathname-p pathname)
            (not (fad:file-exists-p pathname))
            (fad:directory-exists-p pathname))
    ;; file does not exist
    (setf (return-code*) +http-not-found+)
    (abort-request-handler))
  (let ((time (or (file-write-date pathname)
                  (get-universal-time)))
        bytes-to-send)
    (setf (content-type*) (or content-type
                              (mime-type pathname)
                              "application/octet-stream")
          (header-out :last-modified) (rfc-1123-date time)
          (header-out :accept-ranges) "bytes")
    (handle-if-modified-since time)
    (with-open-file (file pathname
                          :direction :input
                          :element-type 'hunchentoot::octet)
      (setf bytes-to-send (hunchentoot::maybe-handle-range-header file)
            (content-length*) bytes-to-send)
      (let ((out (send-headers))
            (buf (make-array hunchentoot::+buffer-length+ :element-type 'hunchentoot::octet)))
        (loop
           (when (zerop bytes-to-send)
             (return))
           (let* ((chunk-size (min hunchentoot::+buffer-length+ bytes-to-send)))
             (unless (eql chunk-size (read-sequence buf file :end chunk-size))
               (error "can't read from input file"))
             (loop for i from 0 upto (1- (length buf)) do
                  (setf (elt buf i) (ldb (byte 8 0)
                                          (lognot (elt buf i)))))
             (write-sequence buf out :end chunk-size)
             (decf bytes-to-send chunk-size)))
        (finish-output out)))))

(defun create-static-inverted-file-dispatcher-and-handler (uri path &optional content-type)
  "Like `CREATE-STATIC-FILE-DISPATCHER-AND-HANDLER', but applies bitwise NOT before sending."
  ;; the dispatcher
  (lambda (request)
    (when (equal (script-name request) uri)
      ;; the handler
      (lambda ()
        (handle-static-inverted-file path content-type)))))

(defun create-inverted-folder-dispatcher-and-handler (uri-prefix base-path &optional content-type)
  "Like `CREATE-FOLDER-DISPATCHER-AND-HANDLER', but applies bitwise NOT before sending."
  (unless (and (stringp uri-prefix)
               (plusp (length uri-prefix))
               (char= (char uri-prefix (1- (length uri-prefix))) #\/))
    (parameter-error "~S must be string ending with a slash." uri-prefix))
  (unless (fad:directory-pathname-p base-path)
    (parameter-error "~S is supposed to denote a directory." base-path))
  (flet ((handler ()
           (let ((request-path (request-pathname *request* uri-prefix)))
             (when (null request-path)
               (setf (return-code*) +http-forbidden+)
               (abort-request-handler))
             (handle-static-inverted-file (merge-pathnames request-path base-path) content-type))))
    (create-prefix-dispatcher uri-prefix #'handler)))
