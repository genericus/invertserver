;;;; invertserver.lisp
;;;;
;;;; Copyright (c) 2015 Corey Wells

(in-package #:invertserver)

(defvar *video-location* (merge-pathnames (make-pathname :directory '(:relative "Videos"))
                                          (user-homedir-pathname)))

(defun standard-setup ()
  (setf (html-mode) :html5)
  (setf *dispatch-table*
      (list
       'dispatch-easy-handlers
       (create-inverted-folder-dispatcher-and-handler "/video/" *video-location*)
       (create-static-file-dispatcher-and-handler "/favicon.ico" #P"./favicon.ico"))))

(defun begin (&key (port 8080) (root #P"./"))
  (standard-setup)
  (start (make-instance 'easy-acceptor :port port
                        :document-root root)))
