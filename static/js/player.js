// @license magnet:?xt=urn:btih:d3d9a9a6595521f9666a5e94cc830dab83b65699&dn=expat.txt Expat
"use strict";

function loadvideo(video) {
    var videoelement = document.getElementById('videoplayer');
    var req = new XMLHttpRequest;
    var vidsrc = '/video/' + video;
    req.open('GET', vidsrc, true);
    req.responseType = 'arraybuffer';

    req.onload = function(e) {
        if (this.status === 200) {
            var arraybuf = this.response;
            var view = new Uint8Array(arraybuf);
            for (var i = 0; i < view.length; i++) {
                view[i] = ~view[i];
            }
            var blb = new Blob([view]);
            var blb_url = URL.createObjectURL(blb);
            videoelement.src = blb_url;
            document.getElementById('throbber').style.display = 'none';
            videoelement.play();
        } else {
            window.alert('Video failed to load: ' + video);
        }
    }

    req.send();
}

function handleswitcher() {
    var videoelement = document.getElementById('videoplayer');
    var selector = document.getElementById('videoselect');
    if (selector.value === 'noselect') return;
    document.getElementById('throbber').style.display = 'block';
    loadvideo(selector.value);
}

function init() {
    document.getElementById('videoselect').onchange = handleswitcher;
}

window.onload = init;
// @license-end
