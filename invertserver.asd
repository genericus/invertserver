;;;; invertserver.asd
;;;;
;;;; Copyright (c) 2015 Corey Wells

(asdf:defsystem #:invertserver
  :description "A webserver that conditionally serves some files using bitwise NOT"
  :author "Corey Wells"
  :license "MIT"
  :depends-on (#:hunchentoot #:cl-who)
  :serial t
  :components ((:file "package")
               (:file "handledispatch")
               (:file "frontend")
               (:file "invertserver")))

